% function for grabbing data from images,
% image could be in any format readable by imread();
% written by : lfeng
% updated by: 2014.5.11

function result = getgraphpt(image_filename, x_is_log, y_is_log)
    img_data = imread(image_filename);
    imagesc(img_data); axis off;
    display('click the point of the original');
    orr_coor = ginput(1);
    xmin_pt = orr_coor(1);
    ymin_pt = orr_coor(2);
    display('click the maximum of the x axis');
    xmax_coor = ginput(1);
    xmax_pt = xmax_coor(1);
    display('click the maximum of the y axis');
    ymax_coor = ginput(1);
    ymax_pt = ymax_coor(2);

    x_min = input('input the minimum of the x axis:');
    x_max = input('input the maximum of the x axis:');
    y_min = input('input the minimum of the y axis:');
    y_max = input('input the maximum of the y axis:');

    stop_flag = 'n';
    counter = 1;
    while  (stop_flag~='y') && (stop_flag~='Y')
       data_pt = ginput(1);
       if x_is_log == 1
        data_pt_x = 10^((data_pt(1)-xmin_pt)/(xmax_pt-xmin_pt)*(log10(x_max)-log10(x_min)))*x_min;
       else
        data_pt_x = (data_pt(1)-xmin_pt)/(xmax_pt-xmin_pt)*(x_max-x_min)+x_min;
       end
       if y_is_log == 1
        data_pt_y = 10^((data_pt(2)-ymin_pt)/(ymax_pt-ymin_pt)*(log10(y_max)-log10(y_min)))*y_min;     
       else
        data_pt_y = (data_pt(2)-ymin_pt)/(ymax_pt-ymin_pt)*(y_max-y_min)+y_min;      
       end
       result(counter,:) = [data_pt_x, data_pt_y];
       counter = counter+1;
       stop_flag = input('stop?(y/n):','s');
       if isempty(stop_flag)
          stop_flag = 'n'; 
       end
    end
end