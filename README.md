The MATLAB function provides a convenient way to capture data from original figures.

An example using the raw data image to get data is listed below:
```
#!matlab

image_filename = 'plot-gallery-spline.png';
x_is_log = 0;
y_is_log = 0;
result = getgraphpt(image_filename, x_is_log, y_is_log)

> click the point of the original
> click the maximum of the x axis
> click the maximum of the y axis
> input the minimum of the x axis:0
> input the maximum of the x axis:10
> input the minimum of the y axis:0.5
> input the maximum of the y axis:3.5
> stop?(y/n):
> stop?(y/n):
> stop?(y/n):
> stop?(y/n):
> stop?(y/n):
> stop?(y/n):
> stop?(y/n):
> stop?(y/n):
> stop?(y/n):Y

```